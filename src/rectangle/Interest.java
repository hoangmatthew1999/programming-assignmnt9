// Java program to create a blank text
// field of definite number of columns.
package rectangle;

import java.awt.event.*;
import javax.swing.*;
class Interest extends JFrame implements ActionListener {
    // JTextField
    static JTextField t;
    static JTextField interestRate;
    static JTextField time;

    // JFrame
    static JFrame f;

    // JButton
    static JButton b;

    // label to display text
    static JLabel l;
    static JLabel errorLabel;


    // default constructor
    Interest(){
        // create a new frame to store text field and button
        f = new JFrame("textfield");

        // create a label to display text

        // create a new button

        // create a object of the text class

        // addActionListener to button

        // create a object of JTextField with 16 columns
        t = new JTextField(16);
        interestRate = new JTextField(16);
        time = new JTextField(16);

        l = new JLabel("nothing entered");
        b = new JButton("submit");
        b.addActionListener(this);
        // create a panel to add buttons and textfield
        JPanel p = new JPanel();

        // add buttons and textfield to panel
        p.add(t);
        p.add(interestRate);
        p.add(time);
        p.add(l);
        p.add(b);



        // add panel to frame
        f.add(p);

        // set the size of frame
        f.setSize(300, 300);

        f.show();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    // main class
    public static void main(String[] args)
    {
        Interest obj = new Interest();
    }

    // if the vutton is pressed
    public void actionPerformed(ActionEvent e)
    {
        String s = e.getActionCommand();
        if (s.equals("submit")) {
                // set the text of the label to the text of the field
                System.out.println(t.getText() + "t field");
                System.out.println(interestRate.getText() + "interest field");
                System.out.println(time.getText() + "time field");

                try {
                    double principleInt = Double.parseDouble(t.getText());//turning this to int }
                    int timeInt = Integer.parseInt(  time.getText() );//time should be int also most interest acrues yearly
                    double interestRateInt = Double.parseDouble(interestRate.getText() );
                    double interest = principleInt * interestRateInt * timeInt;
                    System.out.println(interest);
                    l.setText( String.valueOf(interest) );
                }
                catch(Exception principle){ l.setText("One of the inputs is not an int"); }



                //I = Prt formula used
                // set the text of field to blank


                t.setText(" ");
                interestRate.setText(" ");
                time.setText(" ");

        }
    }
}

package rectangle;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random;
import java.util.ArrayList;
public class buttonRectangle extends JFrame implements ActionListener{
    public JButton button;
    public JButton fewerButton;

    public String operation = "";
    ArrayList<rectangle> rectPosDB = new ArrayList<rectangle>();
    public void paintComponent(Graphics g){
            g.setColor(Color.red);
            Random randObj = new Random();

            int xRandom = randObj.nextInt( getWidth() );
            int yRandom = randObj.nextInt(getHeight() );

            rectangle inputArg = new rectangle(xRandom, yRandom, 50, 50);
            rectPosDB.add(inputArg);
            if(operation.equals("more")) {
                for(int i = 0; i<rectPosDB.size();i++) {// for each elem in db, i add 2 thus doubling it
                    g.fillRect(xRandom, yRandom, 50, 50);
                    g.fillRect(randObj.nextInt( getWidth() ), randObj.nextInt( getHeight() ), 50, 50);
                }
            }
            else if(operation.equals("fewer")){
                int origDBLengthHalved = rectPosDB.size() / 2;
//                int i = 0;
                System.out.println(origDBLengthHalved);
                System.out.println(rectPosDB.size());
                for(int i = 0; i< origDBLengthHalved;i++) {
                    int rectObjIndex = randObj.nextInt( rectPosDB.size() );
                    rectangle randomObj = rectPosDB.get(rectObjIndex);
                    int randomObj_xPos = randomObj.getxPos();
                    int randomObj_yPos = randomObj.getyPos();
                    g.clearRect(randomObj_xPos, randomObj_yPos, 50, 50);
                    rectPosDB.remove(rectObjIndex);

                    rectObjIndex = randObj.nextInt( rectPosDB.size() );
                    randomObj = rectPosDB.get(rectObjIndex);
                    randomObj_xPos = randomObj.getxPos();
                    randomObj_yPos = randomObj.getyPos();
                    g.clearRect(randomObj_xPos, randomObj_yPos, 50, 50);

                }

            }
//            g.fillRect(xRandom + 200, yRandom + 200, 50, 50);
    }
    public void draw(Graphics g){
        g.fillRect(100, 100, 100, 100);
    }

    public void start(){
        setLayout(new BorderLayout());
        button=new JButton();
        button.setText("More");
        button.setPreferredSize(new Dimension(100,50));
        button.addActionListener(this);

        fewerButton = new JButton();
        fewerButton.setText("Fewer");
        fewerButton.setPreferredSize( new Dimension(100,50) );
        fewerButton.addActionListener(this);

        add(button, BorderLayout.SOUTH);
        add(fewerButton,BorderLayout.NORTH);
        setSize(500,500);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
//        check=true;
        if(e.getSource() == button){ operation = "more";}
        else if(e.getSource()== fewerButton){operation = "fewer";}
        repaint();

    }

    public static void main(String args[]){
        buttonRectangle x = new buttonRectangle();
        x.start();
    }
}